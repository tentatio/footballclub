/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

import java.util.Date;

/**
 *
 * @author homeguard
 */
public abstract class Event extends DomainObj{ // Абстрактный класс события для расписания
    private Date Day;
    private String Place;
    private Manager Creator;   //Создатель события
    public Event(Manager Creator, Long Id, Date Day, String Place) {
        super(Id);
        this.Day = Day;
        this.Place = Place;
        this.Creator = Creator;
    }
    
    public Date getDay(){
        return this.Day;
    }
    public String getPlace(){
        return this.Place;
    }
    public Manager getCreator(){
        return this.Creator;
    }
    
    public void setDay(Date Day){
        this.Day = Day;
    }
    public void setPlace(String Place){
        this.Place = Place;
    }
    public void setCreator(Manager Creator){
        this.Creator = Creator;
    }
}
