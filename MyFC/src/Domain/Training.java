/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

import java.util.Date;

/**
 *
 * @author homeguard
 */
public class Training extends Event{
    public enum TrainingType{   //Перечисляем типы тренировок
            Sparring,
            Tactics,
            Technique,
            Stamina,
            Workout
    }
    private TrainingType Type;
    private boolean isMain;
    public Training(Manager Creator, Long Id, Date Day, String Place,
            TrainingType Type, boolean isMain) {
        super(Creator, Id, Day, Place);
        this.Type = Type;
        this.isMain = isMain;
    }
    
    public TrainingType getType(){
        return this.Type;
    }
    public boolean getisMain(){
        return this.isMain;
    }
    
    public void setType(TrainingType Type){
        this.Type = Type;
    }
    public void setisMain (boolean isMain){
        this.isMain = isMain;
    }
}
