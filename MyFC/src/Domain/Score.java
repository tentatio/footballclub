/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

/**
 *
 * @author homeguard
 */
public class Score {    //Создаем класс счета, реализующий паттерн объект - значение
    private int OwnerGoals;
    private int GuestGoals;
    public Score (int OwnerGoals, int GuestGoals){  
        this.OwnerGoals = OwnerGoals;
        this.GuestGoals = GuestGoals;
    }
    
    public int getOwnerGoals(){
        return this.OwnerGoals;
    }
    public int getGuestGoals(){
        return this.OwnerGoals;
    }

    
    @Override
    public boolean equals(Object Other){       //Перегружаем метод для 
        return((Other instanceof Score)&&      //сравнения
                equals((Score)Other));         //паттерн
    }                                          //объект - значение

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.OwnerGoals;
        hash = 17 * hash + this.GuestGoals;
        return hash;
    }
    public boolean equals(Score Other){
        if((this.OwnerGoals==Other.OwnerGoals)
          &&(this.GuestGoals==Other.GuestGoals))
            return true;
        else
            return false;
    }
}

