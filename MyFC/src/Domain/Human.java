/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

/**
 *
 * @author homeguard
 */
public abstract class Human extends DomainObj{   // Абстрактный класс для наследования менеджерам и футболистам
    private String LastName;
    private String FirstName;
    private String Patronym;
    
    public Human (Long Id, String LastName, String FirstName, String Patronym){ // Конструктор штучки
        super(Id);
        this.LastName = LastName;
        this.FirstName = FirstName;
        this.Patronym = Patronym;
    }
    
    public String getLastName (){
        return this.LastName;
    }
    
    public String getFirstName (){
        return this.FirstName;
    }
    
    public String getPatronym (){
        return this.Patronym;
    }
    
    public void setLastName (String LastName){
        this.LastName = LastName;
    }
    
     public void setFirstName (String FirstName){
        this.FirstName = FirstName;
    }
     
      public void setPatronym (String Patronym){
        this.Patronym = Patronym;
    }
}

