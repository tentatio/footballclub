/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

import java.util.ArrayList;

/**
 *
 * @author homeguard
 */
public class Team {
    private static Team Instance = null;
    private static ArrayList <Player> MainTeam =
            new ArrayList <Player> ();
    private static ArrayList <Player> AddTeam =
            new ArrayList <Player> ();
    
    private Team(){
        MainTeam = TeamMapper.findMainTeam();
        AddTeam = TeamMapper.findAddTeam();
    }
    
    public static Team getInstance(){
        if (Instance==null){
            Instance = new Team();
        }
    return Instance;
    }

    public ArrayList<Player> getMainTeam() 
    {
        return MainTeam;
    }
    
    public ArrayList<Player> getAddTeam() 
    {
        return AddTeam;
    }
    
}