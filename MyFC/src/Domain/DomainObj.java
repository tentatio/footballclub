/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

/**
 *
 * @author homeguard
 */
public abstract class DomainObj { // Создаем супертип слоя бизнес логики
    private Long Id;
    
    public DomainObj (Long Id){
        if (Id==null) throw new NullPointerException(); // Проверяем Идишник
        this.Id = Id;
    }
    
    public Long getId(){
        return this.Id;
    }
    
    public void setId(Long Id){
        if (Id==null) throw new NullPointerException();
        this.Id = Id;
    }
}
