/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author homeguard
 */
public class Player extends Human { // описываем класс игрока ФК
    private String Country;
    public enum PositionType { // описываем тип позиций игрока
        Goalkeeper,
        MidDefender,
        RightDefender,
        LeftDefender,
        Midfielder,
        RightMidfielder,
        LeftMidfielder,
        HoldingMidfielder,
        Forward,
        Striker
    };
    
    private PositionType Position;
    private int Rate;
    private Date Birth;
    private boolean isRightFooted; // Сильная нога
    private boolean isHealth; // Травма
    
    public Player (Long Id, String LastName, String FirstName, String Patronym,
            String Country, PositionType Position, int Rate, Date Birth,
            boolean isRightFooted, boolean isHealth){
        super(Id,LastName,FirstName,Patronym);
        
        this.Birth = Birth;
        this.Country = Country;
        this.Position = Position;
        this.Rate = Rate;
        this.isHealth = isHealth;
        this.isRightFooted = isRightFooted;
    }
    
    public boolean isMain(){
        Team r = Team.getInstance();
        for (Player p: r.getMainTeam()){
            if (this.equals(p))
                return true;
        }
        return false;
    }
    
    private Integer getAge() // подсчет возраста игрока
{
    Calendar dob = Calendar.getInstance();
    Calendar today = Calendar.getInstance();
 
    dob.setTime(this.Birth);
    // включает день рождения
    dob.add(Calendar.DAY_OF_MONTH, -1);
 
    int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
    if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR)) {
        age--;
    }
    return age;
}
    
    
    public int getChangeScore(Player Other){    // вычисляем "рейтинг" замены
        int changeScore = 0;
        
        if(this.Country == Other.Country){
            changeScore++;
        }
        
        if (this.Position == Other.Position){
            changeScore+=5;
        }
        
        if (this.isRightFooted == Other.isRightFooted){
            changeScore++;
        }
        
        if (Other.getAge() < 30){
            changeScore+=2;
        }
        else {
            changeScore++;
        }
        
        changeScore+=Other.Rate;
        
        return changeScore;
    }

    public ArrayList <Integer> CheckRecommendations() 
    //Создаем лист с РЕЙТИНГАМИ ПОСЧИТАННЫМИ ВЫШЕ, вопрос как их потом объяснить
    {
        Team Registry=Team.getInstance();
        ArrayList <Player> AddTeamTmp=Registry.getAddTeam();
        for (Player AddTeamTmp1 : AddTeamTmp) {
            getChangeScore(AddTeamTmp1);
            AddTeamTmp.add(getChangeScore(AddTeamTmp1), this);
        }
        
        //lolwut? o_O
        
        
        
        return CheckRecommendations();
    }

    
    public String getCountry (){
        return this.Country;
    }
    public PositionType getPosition(){
        return this.Position;
    }
    public int getRate(){
        return this.Rate;
    }
    public Date getBirth(){
        return this.Birth;
    }
    public boolean getisRightFooted(){
        return this.isRightFooted;
    } 
    public boolean getisHealth(){
        return this.isHealth;
    } 
    
    public void setCountry (String Country){
        this.Country = Country;
    }
    public void setPosition (PositionType Position){
        this.Position = Position;
    }
    public void setRate (int Rate){
        this.Rate = Rate;
    }
    public void setBirth (Date Birth){
        this.Birth = Birth;
    }
    public void setisRightFooted (boolean isRightFooted){
        this.isRightFooted = isRightFooted;
    }
    public void setisHeatlh(boolean isHealth){
        this.isHealth = isHealth;
    }
}

