/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Domain;

import java.util.Date;

/**
 *
 * @author homeguard
 */
public class Match extends Event{
    private String EnemyTeam;
    private Score Result;
    public Match(Manager Creator, Long Id, Date Day, String Place,
            String EnemyTeam, Score Result) {
        super(Creator, Id, Day, Place);
        this.EnemyTeam = EnemyTeam;
        this.Result = Result;
    }
            
    public String getEnemyTeam(){
        return this.EnemyTeam;
    }
    public Score getResult(){
        return this.Result;
    }
    
    public void setEnemyTeam(String EnemyTeam){
        this.EnemyTeam = EnemyTeam;
    }
    public void setResult(Score Result){
        this.Result = Result;
    }
}

